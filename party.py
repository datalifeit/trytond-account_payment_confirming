# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Id


class ConfirmingValueMixin(object):
    __slots__ = ()

    confirming_values = fields.One2Many(
        'account.payment.confirming.set.value', 'origin',
        'Confirmings')

    def get_confirming_fields(self, template):
        for line in self.confirming_values:
            if line.field_set.template == template:
                return line.confirming_fields
        return None


class Party(ConfirmingValueMixin, metaclass=PoolMeta):
    __name__ = 'party.party'


class Address(ConfirmingValueMixin, metaclass=PoolMeta):
    __name__ = 'party.address'

    @classmethod
    def __setup__(cls):
        super(Address, cls).__setup__()
        _value_invisible = ~Eval('context', {}).get('groups', []).contains(
            Id('account', 'group_account'))
        if cls.confirming_values.states.get('invisible'):
            cls.confirming_values.states['invisible'] |= _value_invisible
        else:
            cls.confirming_values.states['invisible'] = _value_invisible
