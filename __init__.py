# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import payment
from . import party


def register():
    Pool.register(
        payment.ConfirmingSet,
        payment.ConfirmingField,
        payment.ConfirmingFieldFieldSet,
        payment.ConfirmingSetValue,
        payment.Journal,
        payment.Group,
        payment.Payment,
        payment.Message,
        party.Party,
        party.Address,
        module='account_payment_confirming', type_='model')
