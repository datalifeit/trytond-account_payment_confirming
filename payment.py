# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
import io
import datetime
import genshi
import genshi.template
import unicodedata
from trytond.model import (Workflow, ModelView, ModelSQL, fields, dualmethod,
    DictSchemaMixin, Index)
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from num2words import num2words


# XXX fix: https://genshi.edgewall.org/ticket/582
from genshi.template.astutil import ASTCodeGenerator, ASTTransformer
if not hasattr(ASTCodeGenerator, 'visit_NameConstant'):
    def visit_NameConstant(self, node):
        if node.value is None:
            self._write('None')
        elif node.value is True:
            self._write('True')
        elif node.value is False:
            self._write('False')
        else:
            raise Exception("Unknown NameConstant %r" % (node.value,))
    ASTCodeGenerator.visit_NameConstant = visit_NameConstant
if not hasattr(ASTTransformer, 'visit_NameConstant'):
    # Re-use visit_Name because _clone is deleted
    ASTTransformer.visit_NameConstant = ASTTransformer.visit_Name


class Journal(metaclass=PoolMeta):
    __name__ = 'account.payment.journal'

    company_party = fields.Function(fields.Many2One('party.party',
            'Company Party'), 'on_change_with_company_party')
    confirming_bank_account_number = fields.Many2One('bank.account.number',
        'Bank Account Number', states={
            'required': Eval('process_method') == 'confirming',
            'invisible': Eval('process_method') != 'confirming',
            },
        domain=[
            ('type', '=', 'iban'),
            ('account.owners', '=', Eval('company_party')),
            ])
    confirming_payable_flavor = fields.Selection([
            (None, '')], 'Payable Flavor',
        states={
            'required': Eval('process_method') == 'confirming',
            'invisible': Eval('process_method') != 'confirming',
            },
        translate=False)
    confirming_field_set = fields.Many2One(
        'account.payment.confirming.field.set',
        'Field set', domain=[
            ('model_name', '=', 'account.payment.journal'),
            ('template', '=', Eval('confirming_payable_flavor'))],
        states={
            'required': Eval('process_method') == 'confirming',
            'invisible': Eval('process_method') != 'confirming',
            })
    confirming_fields = fields.Dict('account.payment.confirming.field',
        'Fields',
        domain=[
            ('sets', '=', Eval('confirming_field_set'))],
        states={
            'invisible': Eval('process_method') != 'confirming',
            })

    @classmethod
    def __setup__(cls):
        super(Journal, cls).__setup__()
        conf_method = ('confirming', 'Confirming')
        if conf_method not in cls.process_method.selection:
            cls.process_method.selection.append(conf_method)

    @classmethod
    def default_company_party(cls):
        pool = Pool()
        Company = pool.get('company.company')
        company_id = cls.default_company()
        if company_id:
            return Company(company_id).party.id

    @fields.depends('company')
    def on_change_with_company_party(self, name=None):
        if self.company:
            return self.company.party.id

    @fields.depends('confirming_payable_flavor', 'confirming_fields')
    def on_change_confirming_payable_flavor(self):
        AttrSet = Pool().get('account.payment.confirming.field.set')
        if self.confirming_payable_flavor:
            attr_set, = AttrSet.search([
                ('model_name', '=', self.__name__),
                ('template', '=', self.confirming_payable_flavor)])
            self.confirming_field_set = attr_set
            self.confirming_fields = {a.name: None
                for a in attr_set.confirming_fields}


def remove_comment(stream):
    for kind, data, pos in stream:
        if kind is genshi.core.COMMENT:
            continue
        yield kind, data, pos


loader = genshi.template.NewTextTemplate(
    os.path.join(os.path.dirname(__file__), 'template'))


class Group(metaclass=PoolMeta):
    __name__ = 'account.payment.group'

    confirming_messages = fields.One2Many('account.payment.confirming.message',
        'origin', 'Confirming Messages', readonly=True,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'invisible': ~Eval('confirming_messages'),
            })

    @classmethod
    def __setup__(cls):
        super(Group, cls).__setup__()
        cls._buttons.update({
            'generate_confirming_message': {
                'invisible': (Eval('_parent_journal', {}).get(
                    'process_method') != 'confirming'),
            },
        })
        if 'generate_message' in cls._buttons:
            cls._buttons.update({
                'generate_message': {
                    'invisible': (Eval('_parent_journal', {}).get(
                        'process_method') == 'confirming'),
                },
            })

    def get_confirming_template_name(self):
        return '%s.txt' % self.journal.confirming_payable_flavor

    def get_confirming_template(self):
        template_name = self.get_confirming_template_name()
        _path = self._get_confirming_template_paths()[template_name]
        with io.open(os.path.join(_path, template_name), 'r',
                encoding='ascii') as tempfile:
            return genshi.template.NewTextTemplate(tempfile.read())

    @classmethod
    def _get_confirming_template_paths(cls):
        return {
            'base': os.path.join(os.path.dirname(__file__), 'template')
        }

    def process_confirming(self):
        assert self.kind == 'payable'

        self.generate_message(_save=False)

    @dualmethod
    @ModelView.button
    def generate_message(cls, groups, _save=True):
        # Bypass account_payment_sepa button
        confirming_groups = [g for g in groups
            if g.journal.process_method == 'confirming']
        other_groups = [g for g in groups
            if g.journal.process_method != 'confirming']

        if confirming_groups:
            cls.generate_confirming_message(confirming_groups, _save=_save)
        if other_groups:
            super(Group, cls).generate_message(other_groups, _save=_save)

    @dualmethod
    @ModelView.button
    def generate_confirming_message(cls, groups, _save=True):
        pool = Pool()
        Message = pool.get('account.payment.confirming.message')

        for group in groups:
            group._check_confirming_fields()
            for payment in group.payments:
                payment._check_confirming_fields()
            tmpl = group.get_confirming_template()
            if not tmpl:
                raise NotImplementedError
            if not group.confirming_messages:
                group.confirming_messages = ()
            message = tmpl.generate(group=group,
                datetime=datetime, normalize=unicodedata.normalize,
                num2words=num2words
                ).filter(remove_comment).render()
            message = Message(message=cls._format_confirming_message(message),
                type='out', state='waiting',
                company=group.company)
            group.confirming_messages += (message,)
        if _save:
            cls.save(groups)

    @classmethod
    def _format_confirming_message(cls, message):
        return message

    def get_confirming_fields(self):
        return self.journal.confirming_fields

    def _check_confirming_fields(self):
        """Extend to validate required fields"""
        pass


class Payment(metaclass=PoolMeta):
    __name__ = 'account.payment'

    def get_confirming_fields(self):
        values = self.party.get_confirming_fields(
            self.journal.confirming_payable_flavor)
        _address = self._get_confirming_address()
        if values is None:
            values = {}
        if _address:
            address_values = _address.get_confirming_fields(
                self.journal.confirming_payable_flavor)
            if address_values is not None:
                values.update(address_values)
        return values

    def _check_confirming_fields(self):
        """Extend to validate required fields"""
        pass

    def _get_confirming_address(self):
        if self.line and self.line.origin \
                and self.line.origin.__name__ == 'account.invoice':
            return self.line.origin.invoice_address
        return self.party.address_get(type='invoice')

    def _get_confirming_contact_mechanisms(self):
        # Each model should format contact mechanisms
        values = {
            'phone': '',
            'fax': '',
            'email': ''
        }
        remaining_types = list(values.keys())
        for mechanism in self.party.contact_mechanisms:
            if mechanism.type in remaining_types:
                values[mechanism.type] = mechanism.value_compact
                remaining_types.remove(mechanism.type)
        return values

    def _get_confirming_bank_account(self):
        if self.party.bank_accounts:
            return self.party.bank_accounts[0]


class Message(Workflow, ModelSQL, ModelView):
    'Confirming Message'
    __name__ = 'account.payment.confirming.message'

    _states = {
        'readonly': Eval('state') != 'draft',
        }
    message = fields.Text('Message', states=_states)
    filename = fields.Function(fields.Char('Filename'), 'get_filename')
    type = fields.Selection([
            ('out', 'OUT'),
            ], 'Type', required=True, states=_states)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        states={
            'readonly': Eval('state') != 'draft',
            })
    origin = fields.Reference('Origin', selection='get_origin',
        states=_states)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('waiting', 'Waiting'),
            ('done', 'Done'),
            ('canceled', 'Canceled'),
            ], 'State', readonly=True,)

    @classmethod
    def __setup__(cls):
        super(Message, cls).__setup__()
        table = cls.__table__()
        cls._transitions |= {
            ('draft', 'waiting'),
            ('waiting', 'done'),
            ('waiting', 'draft'),
            ('draft', 'canceled'),
            ('waiting', 'canceled'),
            }
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'waiting']),
                    'depends': ['state'],
                    },
                'draft': {
                    'invisible': Eval('state') != 'waiting',
                    'depends': ['state'],
                    },
                'wait': {
                    'invisible': Eval('state') != 'draft',
                    'depends': ['state'],
                    },
                'do': {
                    'invisible': Eval('state') != 'waiting',
                    'depends': ['state'],
                    },
                })

        cls._sql_indexes.update({
            Index(table, (table.company, Index.Equality())),
            Index(table, (table.origin, Index.Equality())),
            Index(table, (table.state, Index.Equality())),
            })

    @staticmethod
    def default_type():
        return 'out'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    def get_filename(self, name):
        pool = Pool()
        Group = pool.get('account.payment.group')
        if isinstance(self.origin, Group):
            return self.origin.rec_name + '.txt'

    @staticmethod
    def _get_origin():
        'Return list of Model names for origin Reference'
        return ['account.payment.group']

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
                ('model', 'in', models),
                ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, messages):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, messages):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, messages):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, messages):
        pass


class ConfirmingSet(ModelSQL, ModelView):
    """Account payment Confirming field Set"""
    __name__ = 'account.payment.confirming.field.set'

    name = fields.Char('Name', required=True)
    template = fields.Selection('_get_templates', 'Template', required=True)
    model_name = fields.Char('Model name', required=True, readonly=True)
    confirming_fields = fields.Many2Many(
        'account.payment.confirming.field-field.set',
        'field_set', 'confirming_field', 'confirming_fields')

    @classmethod
    def _get_templates(cls):
        Journal = Pool().get('account.payment.journal')
        return Journal.confirming_payable_flavor.selection


class ConfirmingField(DictSchemaMixin, ModelSQL, ModelView):
    """Account payment Confirming Field"""
    __name__ = 'account.payment.confirming.field'

    sets = fields.Many2Many(
        'account.payment.confirming.field-field.set',
        'confirming_field', 'field_set', 'Sets')


class ConfirmingFieldFieldSet(ModelSQL):
    """Account payment Confirming field - Set"""
    __name__ = 'account.payment.confirming.field-field.set'
    _table = 'account_payment_confirming_field_set_rel'

    confirming_field = fields.Many2One('account.payment.confirming.field',
        'Field', ondelete='CASCADE', required=True)
    field_set = fields.Many2One('account.payment.confirming.field.set',
        'Set', ondelete='CASCADE', required=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()

        cls._sql_indexes.update({
            Index(table, (table.confirming_field, Index.Equality())),
            Index(table, (table.field_set, Index.Equality()))
            })


class ConfirmingSetValue(ModelSQL, ModelView):
    """Account payment Confirming set value"""
    __name__ = 'account.payment.confirming.set.value'

    origin = fields.Reference('Origin', selection='get_origin',
        required=True)
    model_name = fields.Function(fields.Char('Model name'),
        'on_change_with_model_name')
    field_set = fields.Many2One('account.payment.confirming.field.set',
        'field set', required=True, domain=[
            ('model_name', '=', Eval('model_name'))])
    confirming_fields = fields.Dict('account.payment.confirming.field',
        'fields', domain=[
            ('sets', '=', Eval('field_set'))])

    @classmethod
    def _get_origin(cls):
        return [
            'party.party',
            'party.address',
            'account.payment.journal'
        ]

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
                ])
        return [('', '')] + [(m.model, m.name) for m in models]

    @fields.depends('origin')
    def on_change_with_model_name(self, name=None):
        if self.origin:
            return self.origin.__name__

    @fields.depends('field_set', '_parent_field_set.confirming_fields')
    def on_change_field_set(self):
        if self.field_set:
            self.confirming_fields = {a.name: None
                for a in self.field_set.confirming_fields}
